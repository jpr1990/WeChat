$(function(){
	//关闭alert框
	$(".alert > .close").bind("click",function(){
		$(this).closest(".alert").fadeOut();
	});
	//弹出确认提示
	$(".confirm").click(function(){
		return confirm('确定要执行此操作么？');
	});
	//ajax-get提交
	$(".ajax-get").click(function(){
		var target;
		var that = this;
		if (target = $(this).attr('href')) {
			$.get(target).success(function(data){
				alert(data.info);
				if (data.url) {
					location.href=data.url;
				}else if($(that).hasClass('no-refresh')){
					$('#top-alert').find('button').click();
				}else{
					location.reload();
				}
			});
		}
		return false;
	});
	$('.ajax-post').click(function(){
		var target,query,form;
		var target_form = $(this).attr('target-form');
		var that = this;
		var nead_confirm=false;
		if(($(this).attr('type')=='submit') || (target = $(this).attr('href')) || (target = $(this).attr('url'))){
			form = $('.'+target_form);
			if (form.get(0)==undefined){
				return false;
			}else if (form.get(0).nodeName=='FORM'){
				if($(this).attr('url') !== undefined){
					target = $(this).attr('url');
				}else{
					target = form.get(0).action;
				}
				query = form.serialize();
			}else if( form.get(0).nodeName=='INPUT' || form.get(0).nodeName=='SELECT' || form.get(0).nodeName=='TEXTAREA') {
				form.each(function(k,v){
					if(v.type=='checkbox' && v.checked==true){
						nead_confirm = true;
					}
				});
				if (nead_confirm && $(this).hasClass('confirm')) {
					if(!confirm('确认要执行该操作吗?')){
						return false;
					}
				}
				query = form.serialize();
			}else {
				query = form.find('input,select,textarea').serialize();
			}
			$(that).addClass('disabled').attr('autocomplete','off').prop('disabled',true);
			$.post(target,query).success(function(data){
				alert(data.info);
				$(that).removeClass('disabled').prop('disabled',false);
				if (data.status == 1) {
					if (data.url) {
						location.href=data.url;
					}else if($(that).hasClass('no-refresh')){
						
					}else{
						location.reload();
					}
				}else {
					if (data.url) {
						location.href=data.url;
					}
				}
			});
		}
		return false;
	});
});