-- -----------------------------
-- Table structure for `cgo_shop_config`
-- Backup date 2014-03-19 10:13:31
-- -----------------------------
DROP TABLE IF EXISTS `cgo_shop_config`;
CREATE TABLE IF NOT EXISTS `cgo_shop_config` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `type` tinyint(3) unsigned NOT NULL,
  `title` varchar(50) NOT NULL,
  `value` text NOT NULL,
  `create_time` int(10) unsigned NOT NULL,
  `update_time` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

