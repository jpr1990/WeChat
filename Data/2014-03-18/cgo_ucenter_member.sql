-- -----------------------------
-- Table structure for `cgo_ucenter_member`
-- Backup date 2014-03-18 15:32:36
-- -----------------------------
DROP TABLE IF EXISTS `cgo_ucenter_member`;
CREATE TABLE IF NOT EXISTS `cgo_ucenter_member` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `username` char(16) NOT NULL COMMENT '用户名',
  `password` char(40) NOT NULL COMMENT '密码',
  `email` char(32) NOT NULL COMMENT '用户邮箱',
  `mobile` char(15) NOT NULL COMMENT '用户手机',
  `reg_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '注册时间',
  `reg_ip` bigint(20) NOT NULL DEFAULT '0' COMMENT '注册IP',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `status` tinyint(4) DEFAULT '0' COMMENT '用户状态',
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `email` (`email`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- -----------------------------
-- Records of `cgo_ucenter_member` Rows:2
-- -----------------------------
INSERT INTO `cgo_ucenter_member` VALUES ('1', 'admin', 'a8027a837bf48913e013bcb1e269d02283a61e14', 'cashkj@163.com', '', '1394761155', '0', '1394761155', '1'),
('2', 'root', 'a8027a837bf48913e013bcb1e269d02283a61e14', 'chenjxlg@163.com', '', '0', '0', '0', '1');