<?php 
namespace Home\Controller;
use Think\Controller;

class HomeController extends Controller {
	//初始化方法
	protected function _initialize() {
		/* 读取数据库中的配置 */
		$config =   S('DB_CONFIG_DATA');
		if(!$config){
			$config =   api('Config/lists');
			S('DB_CONFIG_DATA',$config);
		}
		C($config); //添加配置
		$this->title();
	}
	
	protected function title($title = '') {
		!empty($title) && $title = $title.' - ';
		$this->assign('meta_title', $title. C('WEB_SITE_TITLE'));
	}
}