<?php
/**
 * C.Jango 公共配置文件
 * @author 、小陈叔叔 <www.cjango.com>
 */
return array(
	/* 加载全局路由配置 */
	'LOAD_EXT_CONFIG'=> 'router',

	/* 用户相关设置 */
	'USER_MAX_CACHE'     => 1000, //最大缓存用户数
	'USER_ADMINISTRATOR' => 1, //管理员用户ID
	
	/* 数据缓存设置 */
	'DATA_CACHE_TYPE'   => 'File',   // 数据缓存类型
	'DATA_CACHE_PREFIX'    => '',    // 缓存前缀

	/* 模块相关配置 */
	'DEFAULT_MODULE'     => 'Home',  //默认模块
    'MODULE_DENY_LIST'   => array('Common','User'), //禁止访问的模块
    'MODULE_ALLOW_LIST'  => array('Home', 'Admin'),  //允许访问的模块

	/* URL配置 */
	'URL_CASE_INSENSITIVE' => true, //默认false 表示URL区分大小写 true则表示不区分大小写
	'URL_MODEL'            => 2,    //URL模式
	'VAR_URL_PARAMS'       => '',   // PATHINFO URL参数变量
	'URL_PATHINFO_DEPR'    => '/',  //PATHINFO URL分割符

	/* 全局过滤配置 */
	'DEFAULT_FILTER' => '',         //全局过滤函数

	/* 数据库配置 */
	'DB_TYPE'   => 'mysql',         // 数据库类型
	'DB_HOST'   => 'localhost',     // 服务器地址
	'DB_NAME'   => 'wechat',        // 数据库名
	'DB_USER'   => 'root',          // 用户名
	'DB_PWD'    => '123456',        // 密码
	'DB_PORT'   => '3306',          // 端口
	'DB_PREFIX' => 'cgo_',          // 数据库表前缀

	/* 数据加密 */
    'DATA_AUTH_KEY'      => 'Z6$+mXf*Fa3<c.NtY~1[:T(e,P"J0wL|knH_sE!S',
);