<?php
namespace Admin\Controller;
use Think\Controller;
class AdminController extends Controller {

	function _initialize() {
		// 获取当前用户ID
		define('UID',is_login());
		if(!UID){
			cookie('__init_page__',U());
			$this->redirect('Public/login');
		}
		/* 读取数据库中的配置 */
		$config =   S('DB_CONFIG_DATA');
		if(!$config){
			$config =   api('Config/lists');
			S('DB_CONFIG_DATA',$config);
		}
		C($config); //添加配置
	}
	final function ok($message = '操作成功！', $callbackType = '', $refTab = '', $forwardUrl = '') {
		$code['statusCode'] = 200;
		$code['message']    = $message;
		if (!empty($refTab)) {
			$code['navTabId']   = $refTab;
		}
		if ($callbackType == 'close') {
			$code['callbackType'] = 'closeCurrent';
		}elseif ($callbackType == 'forward') {
			$code['callbackType'] = 'forward';
			$code['forwardUrl']   = $forwardUrl;
		}
		$this->ajaxReturn($code);
	}
	
	final function err($message = '操作失败！', $callbackType = '', $forwardUrl = '', $refTab = '') {
		$code['statusCode'] = 300;
		$code['message']    = $message;
		if (!empty($refTab)) {
			$code['navTabId']   = $refTab;
		}
		if ($callbackType == 'close') {
			$code['callbackType'] = 'closeCurrent';
		}elseif ($callbackType == 'forward') {
			$code['callbackType'] = 'forward';
			$code['forwardUrl']   = $forwardUrl;
		}
		$this->ajaxReturn($code);
	}
	
	final function lists($model, $map = '', $order = '') {
		if (is_string($model)) {
			$model = M($model);
		}
		$pageNum = I('post.pageNum', 1);
		$numPerPage = I('post.numPerPage', 30);
		$list = $model->where($map)->page($pageNum, $numPerPage)->order($order)->select();
		$this->assign('__total__', $model->where($map)->count());
		$this->assign('__list__', $list);
	}
}