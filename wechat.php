<?php
define('APP_DEBUG',True);
// 应用模式 API模式
define('APP_MODE', 'api');
//绑定应用名称
define('BIND_MODULE', 'Wechat');
// 定义缓存目录
define('RUNTIME_PATH', './Runtime/');
// 定义应用目录
define('APP_PATH','./Application/');
// 不生成目录安全文件
define('BUILD_DIR_SECURE', false);
// 加载核心文件
require './ThinkPHP/ThinkPHP.php';

function load_config($file){
	return include $file;
}